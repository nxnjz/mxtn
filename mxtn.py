#!/usr/bin/env python3

import xml.etree.ElementTree as ET
import sys, argparse


class Dictlist(dict):
    def __setitem__(self, key, value):
        try:
            self[key]
        except KeyError:
            super(Dictlist, self).__setitem__(key, [])
        self[key].append(value)


def mcxmlparse(xml="", inputtype="filename"):
    if inputtype == "filename":
        tree = ET.parse(xml)
    elif inputtype == "string":
        tree = ET.fromstring(xml)

    xroot = tree.getroot()

    addrport = Dictlist()
    for xelem in xroot:
        if xelem.tag == "host":
            addr = xelem[0].attrib["addr"]
            for p in xelem[1]:
                if p.tag == "port":
                    addrport[addr] = p.attrib["portid"]

    return addrport


def print_nmapcmds(
    addrport, nmap_flags, outfile, nmap_output,
):
    outfile = open(outfile, "a")
    if nmap_output == "xml":
        output_argsf = "-oX nmap-{}.xml"
    elif nmap_output == "nmap":
        output_argsf = "-oN nmap-{}.nmap"
    elif nmap_output == "gnmap":
        output_argsf = "-oG nmap-{}.gnmap"
    elif nmap_output == "all":
        output_argsf = "-oA nmap-{}"
    else:
        output_argsf = ""
    for pair in addrport.items():
        print(
            "nmap %s -p %s %s %s"
            % (nmap_flags, ",".join(pair[1]), output_argsf.format(pair[0]), pair[0]),
            file=outfile,
        )
    outfile.close()


if __name__ == "__main__":
    cli = argparse.ArgumentParser(
        description="Parse Masscan XML output into nmap commands"
    )
    cli.add_argument("-i", "--input", help="Masscan XML filename", required=True)
    cli.add_argument(
        "-o",
        "--output",
        help="Output filename. Default: stdout",
        default="/dev/stdout",
    )
    cli.add_argument(
        "-f",
        "--nmap-flags",
        help="Nmap flags. Use the syntax -f='-a -b -cD'. Default: -sV -sC -T4",
        default="-sV -sC -T4",
    )
    cli.add_argument(
        "-n",
        "--nmap-output",
        help="Nmap output format (xml|nmap|gnmap|all). Default: None",
        default="none",
    )
    clia = cli.parse_args()

    parsed = mcxmlparse(clia.input)
    print_nmapcmds(
        parsed,
        nmap_flags=clia.nmap_flags,
        outfile=clia.output,
        nmap_output=clia.nmap_output.lower(),
    )
